FROM python

ENV SRC=/src
WORKDIR $SRC

ADD . $SRC

RUN pip install --editable .[dev]
RUN pip install tox pudb


CMD python -m orpverrot --help

import io
import enum
import itertools
import socket

from twisted.internet.defer import Deferred
from twisted.protocols.policies import ProtocolWrapper
from twisted.protocols.policies import WrappingFactory
from twisted.logger import Logger

_logger = Logger()


class Command(enum.Enum):
    TCP_STREAM = 0x01
    TCP_BIND = 0x02
    UDP = 0x03


class AddrType(enum.Enum):
    IPv4 = 0x01
    DOMAIN = 0x03
    IPv6 = 0x04


class Status(enum.Enum):
    GRANTED = 0x00
    GENERAL_FAILURE = 0x01
    DISALLOWED = 0x02
    NETWORK_UNREACHABLE = 0x03
    HOST_UNREACHABLE = 0x04
    REFUSED = 0x05
    TTL_EXPIRED = 0x06
    UNSUPPORTED_COMMAND = 0x07
    UNSUPPORTED_ADDRESS = 0x08


class NoAuth(object):
    CODE = 0x00

    @classmethod
    def do_auth(self, protocol):
        protocol.connect_request()


class UserPasswd(object):
    CODE = 0x02
    SUCCESS = 0x00
    VERSION = 0x01

    def __init__(self, user, passwd):
        self.user = user.encode('ascii')
        self.passwd = passwd.encode('ascii')

    def do_auth(self, protocol):
        # TODO: write version + name length + name + passwd length + passwd
        pass

    def handle_response(self, protocol, data):
        (version, status) = data
        assert version == self.VERSION
        if status != self.SUCCESS:
            pass  # FIXME: log error, lose connection
        else:
            protocol.connect_request()


class Socks5Protocol(ProtocolWrapper):

    VERSION = 0x05
    RESERVED = 0x00
    UNACCEPTABLE = 0xff

    def __init__(self, factory, wrappedProtocol):
        ProtocolWrapper.__init__(self, factory, wrappedProtocol)
        self.state = self.State.INIT
        self._buffer = io.BytesIO()
        self._write = self._buffer.write
        self._recieve = None
        self.auth_method = None

    def connectionMade(self):
        self.handshake()

    def handshake(self):
        self._recieve = self.auth
        msg = bytes(itertools.chain(
            [self.VERSION, len(self.factory.auth_methods)],
            self.factory.auth_methods.keys()
        ))
        self.transport.write(msg)

    def dataReceived(self, data):
        # TODO: try-except and log error
        self._recieve(data)

    def auth(self, data):
        self._recieve = self.handle_auth
        (version, server_choice) = data
        assert version == self.VERSION
        if server_choice == self.UNACCEPTABLE:
            pass  # FIXME log error, lose connection
        else:
            self.auth_method = self.factory.auth_methods[server_choice]
            self.auth_method.do_auth(self)

    def handle_auth(self, data):
        self.auth_method.handle_response(self)

    def connect_request(self):
        self._recieve = self.acknowledge
        msg = bytes(itertools.chain(
            [
                self.VERSION,
                self.factory.command.value,
                self.RESERVED,
                self.factory.address_type.value,
            ],
            self.factory.address,
        )) + self.factory.port.to_bytes(2, byteorder='big')
        self.transport.write(msg)

    def acknowledge(self, data):
        self._recieve = super().dataReceived
        (version, status, reserved, *_) = data
        assert version == self.VERSION
        assert reserved == self.RESERVED
        if status != Status.GRANTED:
            pass  # FIXME: log error, lose connection
        self._deplete_buffer()

    def write(self, data):
        self._write(data)

    def writeSequence(self, iovec):
        self.write(b"".join(iovec))

    def _deplete_buffer(self):
        self._buffer.seek(0)
        self._write = super().write
        self.write(self._buffer.read())
        del self._buffer


class Socks5ClientFactory(WrappingFactory):
    protocol = Socks5Protocol
    noisy = False

    def __init__(self, wrappedFactory, target, port,
                 address_type=AddrType.IPv4,
                 command=Command.TCP_STREAM,
                 auths=[NoAuth]):
        WrappingFactory.__init__(self, wrappedFactory)
        self.command = command
        self.address_type = address_type
        self.auth_methods = {method.CODE: method for method in auths}
        self.address = self._build_address(target)
        self.port = port
        self.done = Deferred()

    def _build_address(self, target):
        if self.address_type is AddrType.IPv4:
            return list(
                socket.inet_pton(socket.AF_INET, target)
            )
        elif self.address_type is AddrType.DOMAIN:
            return [
                len(target)
            ] + list(
                target.encode('ascii')
            )
        elif self.address_type is AddrType.IPv6:
            return list(
                socket.inet_pton(socket.AF_INET6, target)
            )

    def clientConnectionFailed(self, connector, reason):
        _logger.debug(
            'connection failed: {reason}',
            reason=reason.getErrorMessage())
        self.done.errback(reason)

    def clientConnectionLost(self, connector, reason):
        _logger.debug(
            'connection lost: {reason}',
            reason=reason.getErrorMessage())
        self.done.callback(None)

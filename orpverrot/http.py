
import twisted.logger
import twisted.internet.reactor
import twisted.internet.protocol
import twisted.internet.endpoints
import twisted.web.http

import orpverrot.socks5


_logger = twisted.logger.Logger()


class ProxyClient(twisted.web.http.HTTPClient):
    _finished = False

    def __init__(self, command, rest, version, headers, data, father):
        self.father = father
        self.command = command
        self.rest = rest
        if b"proxy-connection" in headers:
            del headers[b"proxy-connection"]
        headers[b"connection"] = b"close"
        headers.pop(b'keep-alive', None)
        self.headers = headers
        self.data = data

    def connectionMade(self):
        self.sendCommand(self.command, self.rest)
        for header, value in self.headers.items():
            self.sendHeader(header, value)
        self.endHeaders()
        self.transport.write(self.data)

    def handleStatus(self, version, code, message):
        self.father.setResponseCode(int(code), message)

    def handleHeader(self, key, value):
        # t.web.server.Request sets default values for these headers in its
        # 'process' method. When these headers are received from the remote
        # server, they ought to override the defaults, rather than append to
        # them.
        if key.lower() in [b'server', b'date', b'content-type']:
            self.father.responseHeaders.setRawHeaders(key, [value])
        else:
            self.father.responseHeaders.addRawHeader(key, value)

    def handleResponsePart(self, buffer):
        self.father.write(buffer)

    def handleResponseEnd(self):
        if not self._finished:
            self._finished = True
            self.father.finish()
            self.transport.loseConnection()


class ProxyClientFactory(twisted.internet.protocol.ClientFactory):

    protocol = ProxyClient
    noisy = False

    def __init__(self, command, rest, version, headers, data, father):
        self.father = father
        self.command = command
        self.rest = rest
        self.headers = headers
        self.data = data
        self.version = version

    def buildProtocol(self, addr):
        return self.protocol(
            self.command, self.rest, self.version,
            self.headers, self.data, self.father
        )

    def clientConnectionFailed(self, connector, reason):
        """
        Report a connection failure in a response to the incoming request as
        an error.
        """
        self.father.setResponseCode(501, b"Gateway error")
        self.father.responseHeaders.addRawHeader(b"Content-Type", b"text/html")
        self.father.write(b"<H1>Could not connect</H1>")
        self.father.finish()


class ReverseProxyRequest(twisted.web.http.Request):

    # proxyClientFactoryClass = ProxyClientFactory

    def __init__(self, channel,
                 queued=twisted.web.http._QUEUED_SENTINEL,
                 reactor=twisted.internet.reactor):
        twisted.web.http.Request.__init__(self, channel, queued)
        self.reactor = reactor

    def process(self):
        if b'TWARFSESSIONID' not in self.received_cookies:
            self.addCookie(b'TWARFSESSIONID', b'0000000000')
            self.redirect(self.uri)
            self.finish()
            return
        self.requestHeaders.setRawHeaders(
            b"host",
            [self.channel.factory.host.encode('ascii')]
        )
        factory = ProxyClientFactory(
            self.method, self.uri, self.clientproto, self.getAllHeaders(),
            self.content.read(), self
        )
        factory = orpverrot.socks5.Socks5ClientFactory(
            factory,
            self.channel.factory.host, self.channel.factory.port,
            address_type=orpverrot.socks5.AddrType.DOMAIN
        )
        self.channel.factory.endpoint.connect(factory)


class ReverseProxy(twisted.web.http.HTTPChannel):
    """
    Implements a simple reverse proxy.

    For details of usage, see the file examples/reverse-proxy.py.
    """

    requestFactory = ReverseProxyRequest


class ProxyFactory(twisted.internet.protocol.ServerFactory):

    protocol = ReverseProxy
    noisy = False

    def __init__(self,
                 proxy_host: str, proxy_port: int,
                 target_host: str, target_port: int,
                 reactor=twisted.internet.reactor):
        self.host = target_host
        self.port = target_port

        self.endpoint = twisted.internet.endpoints.TCP4ClientEndpoint(
            reactor,
            proxy_host,
            proxy_port,
        )

    def log(self, *args, **kwargs):
        pass

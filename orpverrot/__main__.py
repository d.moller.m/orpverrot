
import sys
import argparse
import logging.config

import twisted.internet.reactor
import twisted.logger

import orpverrot.proxy
import orpverrot.http


def _logging_cfg(verbosity: int):
    return {
        'version': 1,
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
            },
            'orpverrot': {
                'handlers': ['null'],
                'level': 'DEBUG' if verbosity else 'INFO',
                'propagate': True,
                'qualname': 'orpverrot',
            },
        },
        'handlers': {
            'console': {
                'formatter': 'debug' if verbosity else 'default',
                'class': 'logging.StreamHandler',
                'stream': sys.stdout,
            },
            'null': {
                'formatter': 'default',
                'class': 'logging.NullHandler',
            },
        },
        'formatters': {
            'default': {
                'format':
                    '%(levelname)s:%(name)s:%(message)s',
            },
            'debug': {
                'format':
                    '%(asctime)s.%(msecs)03d %(levelname)s '
                    '[%(name)s:%(funcName)s #%(lineno)s] %(message)s',
                'datefmt': '%H:%M:%S',
            },
        },
    }


def _logger_cfg(verbosity: int):
    return [
        twisted.logger.FileLogObserver(
            sys.stdout,
            twisted.logger.formatEventAsClassicLogText),
    ]


def _get_parser():
    def ipv4_port_type(port: str) -> int:
        x = int(port)
        if x > 65535 or x < 0:
            raise argparse.ArgumentTypeError(
                "Port must be between 0 and 65535")
        return x

    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=ipv4_port_type)
    parser.add_argument('proxy_host')
    parser.add_argument('proxy_port', type=ipv4_port_type)
    parser.add_argument('target_host')
    parser.add_argument('target_port', type=ipv4_port_type)
    parser.add_argument('--verbose', '-v', action='count')
    parser.add_argument('--http', '-H', action='store_true')
    return parser


def main():
    parser = _get_parser()
    args = parser.parse_args()
    logging.config.dictConfig(
        _logging_cfg(args.verbose)
    )
    twisted.logger.globalLogBeginner.beginLoggingTo(
        _logger_cfg(args.verbose)
    )

    factory = (
        orpverrot.http.ProxyFactory
        if args.http
        else orpverrot.proxy.ProxyFactory
    )

    twisted.internet.reactor.listenTCP(
        args.port,
        factory(
            args.proxy_host, args.proxy_port,
            args.target_host, args.target_port
        )
    )
    try:
        twisted.logger.Logger().info(
            'Running orpverrot at port {port}', port=args.port)
        twisted.internet.reactor.run()
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()

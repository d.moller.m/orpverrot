
import io

import twisted.internet.reactor
import twisted.internet.protocol
import twisted.internet.endpoints
import twisted.logger

import orpverrot.socks5


_logger = twisted.logger.Logger()


class ForwardingProtocol(twisted.internet.protocol.Protocol):

    def connectionMade(self):
        self.factory.father.unblock()

    def dataReceived(self, data):
        _logger.debug(
            "Forwarding response {response}",
            response=data
        )
        self.factory.father.write(data)

    def connectionLost(self, reason):
        self.factory.father.loseConnection()

    def loseConnection(self):
        _logger.debug("Closing target connection")
        self.transport.loseConnection()

    def write(self, data):
        _logger.debug(
            "Forwarding request {request}",
            request=data
        )
        self.transport.write(data)


class ForwardingFactory(twisted.internet.protocol.ClientFactory):

    protocol = ForwardingProtocol
    noisy = False

    def __init__(self, father):
        self.father = father

    def buildProtocol(self, what):
        protocol = super().buildProtocol(what)
        self.father.child = protocol
        return protocol


class ReverseProxy(twisted.internet.protocol.Protocol):

    def __init__(self,
                 reactor=twisted.internet.reactor):
        self.reactor = reactor
        self.child = None
        self._handle_data = self.block
        self._buffer = io.BytesIO()

    def block(self, data):
        _logger.debug("buffering {data}", data=data)
        self._buffer.write(data)

    def unblock(self):
        _logger.debug("unblocking")
        self._buffer.seek(0)
        self._handle_data = self.child.write
        self._handle_data(self._buffer.read())
        del self._buffer

    def connectionMade(self):
        factory = orpverrot.socks5.Socks5ClientFactory(
            ForwardingFactory(self),
            self.factory.target_host, self.factory.target_port,
            address_type=orpverrot.socks5.AddrType.DOMAIN
        )
        self.deferred = self.factory.endpoint.connect(factory)

    def dataReceived(self, data):
        self._handle_data(data)

    def loseConnection(self):
        _logger.debug("Closing source connection")
        self.transport.loseConnection()

    def connectionLost(self, reason):
        self.child.loseConnection()

    def write(self, data):
        self.transport.write(data)


class ProxyFactory(twisted.internet.protocol.ServerFactory):

    protocol = ReverseProxy
    noisy = False

    def __init__(self,
                 proxy_host: str, proxy_port: int,
                 target_host: str, target_port: int,
                 reactor=twisted.internet.reactor):
        _logger.info(
            'Initiating factory towards {target_host}:{target_port} '
            'through {proxy_host}:{proxy_port}',
            target_host=target_host, target_port=target_port,
            proxy_host=proxy_host, proxy_port=proxy_port
        )
        self.proxy_host = proxy_host
        self.proxy_port = proxy_port
        self.target_host = target_host
        self.target_port = target_port

        self.endpoint = twisted.internet.endpoints.TCP4ClientEndpoint(
            reactor,
            self.proxy_host,
            self.proxy_port,
        )

    def log(self, *args, **kwargs):
        pass

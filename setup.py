
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='orpverrot',
    version='0.1',
    description='Tor Reverse Proxy',
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='D. Möller',
    author_email='d.moller.m@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=['twisted'],
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage', 'mypy', 'pytest'],
    },
    entry_points={
        'console_scripts': [
            'orpverrot=orpverrot.__main__:main',
        ],
    },
)
